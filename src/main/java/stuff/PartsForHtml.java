package stuff;

/**
 * Created by VV on 12.02.2018.
 */
public class PartsForHtml {

    public PartsForHtml() {
    }

    public String getMainPage() {
        return mainPage;
    }

    private String mainPage = "<!doctype html>\n" +
            "<html>\n" +
            "<head>\n" +
            "    <meta charset=\"UTF-8\">\n" +
            "    <meta name=\"viewport\"\n" +
            "          content=\"width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0\">\n" +
            "    <meta http-equiv=\"X-UA-Compatible\" content=\"ie=edge\">\n" +
            "    <title>Tinder4</title>\n" +
            "    <!--<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\" integrity=\"sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u\" crossorigin=\"anonymous\">-->\n" +
            "    <link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css\" integrity=\"sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm\" crossorigin=\"anonymous\">\n" +
            "</head>\n" +
            "    <body>\n" +
            "    <div class=\"container\">\n" +
            "        <div class=\"mx-auto\">\n" +
            "            <img src=\"http://n1s2.starhit.ru/16/0f/2d/160f2d135c284cbcd2fa6eb9cf8b579a/480x497_1_811abb8258505b2fbf996b541c600f7c@480x497_0xc0a8399a_20345401521468857149.jpeg\" alt=\"Kseniya\" class=\"rounded-circle\">\n" +
            "        </div>\n" +
            "        <div>\n" +
            "            <button type=\"button\" class=\"btn btn-primary btn-lg floated left ml-1 \">Yes</button>\n" +
            "            <button type=\"button\" class=\"btn btn-danger btn-lg mr-1 floated right\">No</button>\n" +
            "        </div>\n" +
            "    </div>\n" +
            "    </body>\n" +
            "</html>";

}
